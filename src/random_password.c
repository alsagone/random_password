#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../include/random_password.h"

#define TRUE (1==1)
#define FALSE (1==0)

#define UPPERCASE 2
#define LOWERCASE 3
#define SYMBOL 4
#define DIGIT 5

//Returns a random number in [min ; max]
int my_rand(int min, int max)
{
  int n = rand()%(max+1-min) + min ;
  return n ;
}

//Returns a random char
char random_char(int type)
{
  int min = 0 ;
  int max = 0 ;
  int c = 0 ;

  switch (type)
  {
    case UPPERCASE : min = 65 ; max = 90 ; break ;
    case LOWERCASE : min = 97 ; max = 122 ; break ;
    case SYMBOL : min = 58 ; max = 64 ; break ;
    case DIGIT : min = 48 ; max = 57 ; break ;
    default : fprintf(stderr, "Unknown type\n") ; exit(1) ; break ;
  }

  c = my_rand(min,max) ;
  return (char) c ;
}

int type (char c)
{
  int t ;
  int c_int = (int) c ;

  if ((c_int >= 65) && (c_int <= 90))
  {
    t = UPPERCASE ;
  }
  else if ((c_int >= 97) && (c_int <= 122))
  {
    t = LOWERCASE ;
  }

  else if ((c_int >= 58) && (c_int <= 64))
  {
    t = SYMBOL ;
  }

  else if ((c_int >= 48) && (c_int <= 57))
  {
    t = DIGIT ;
  }

  else if (c == 32)
  {
    t = SPACE ;
  }

  else
  {
    fprintf(stderr, "Unknown type -> %c\n",c);
    exit(1) ;
  }

  return t ;
}
